using System.Collections.Generic;

namespace LSerialObserver.Domain.Observer
{
    public class ObserverService
    {
        private readonly List<Observer> _list = new();

        public void Add(Observer observer)
        {
            _list.Add(observer);
        }

        public List<Observer> All()
        {
            return _list;
        }
    }
}
