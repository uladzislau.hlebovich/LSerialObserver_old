using System;

namespace LSerialObserver.Domain.Connection
{
    public interface IConnection
    {
        public bool IsConnected { get; }
        public void Start();
        public void Stop();

        public event Action<IConnection, byte[]> DataReceived;
    }
}
