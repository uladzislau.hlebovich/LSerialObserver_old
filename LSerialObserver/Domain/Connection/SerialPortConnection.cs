using System;
using System.IO.Ports;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace LSerialObserver.Domain.Connection
{
    public class SerialPortOptions
    {
        public int BaudRate = 9600;
        public int DataBits = 8;
        public string Device;
        public Parity Parity = Parity.None;
        public StopBits StopBits = StopBits.One;
    }

    [JsonObject(MemberSerialization.OptIn)]
    public class SerialPortConnection : IConnection
    {
        private readonly SerialPort _serialPort;

        [JsonConstructor]
        public SerialPortConnection(SerialPortOptions options)
        {
            Options = options;

            _serialPort = new SerialPort(options.Device, options.BaudRate, options.Parity, options.DataBits,
                    options.StopBits)
                {ReadTimeout = 500, WriteTimeout = 500};

            _serialPort.DataReceived += HandleSerialPortDataReceived;
        }

        [JsonProperty] public SerialPortOptions Options { get; }

        public bool IsConnected => _serialPort.IsOpen;
        public event Action<IConnection, byte[]> DataReceived;

        public void Start()
        {
            _serialPort.Open();
        }

        public void Stop()
        {
            _serialPort.Close();
        }

        public static Task<string[]> AvailableDevices()
        {
            return Task.FromResult(SerialPort.GetPortNames());
        }

        private void HandleSerialPortDataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            var bytes = Encoding.ASCII.GetBytes(_serialPort.ReadExisting());
            DataReceived?.Invoke(this, bytes);
        }
    }
}
