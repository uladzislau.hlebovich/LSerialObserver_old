using LSerialObserver.WebUI;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace LSerialObserver
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args)
        {
            return Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseWebRoot("WebUI/wwwroot");
                    webBuilder.UseStartup<Startup>();
                });
        }
    }
}
