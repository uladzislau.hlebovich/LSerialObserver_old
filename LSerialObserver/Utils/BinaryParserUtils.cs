using System;
using System.Collections;
using System.Collections.Generic;

namespace LSerialObserver.Utils
{
    public static class BinaryParserUtils
    {
        public static long SignedInteger(List<bool> bits)
        {
            var bytes = new byte[8];
            new BitArray(bits.ToArray()).CopyTo(bytes, 0);
            return BitConverter.ToInt64(bytes);
        }
    }
}
